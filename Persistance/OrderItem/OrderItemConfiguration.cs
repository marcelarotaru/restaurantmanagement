﻿using RestaurantManagement.Domain.OrderItem;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantManagement.Persistance.OrderItem
{
    public class OrderItemConfiguration
        : EntityTypeConfiguration<OrderItemEntity>
    {
        public OrderItemConfiguration()
        {
            HasKey(p => p.Id);

            HasRequired(p => p.Order);

            HasRequired(p => p.Item);
        }
    }
}
