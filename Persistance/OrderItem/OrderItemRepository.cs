﻿using RestaurantManagement.Application.Interfaces.Persistance;
using RestaurantManagement.Domain.OrderItem;
using RestaurantManagement.Persistance.Common;

namespace RestaurantManagement.Persistance.OrderItem
{
    public class OrderItemRepository
        : Repository<OrderItemEntity>,
        IOrderItemRepository
    {
        public OrderItemRepository(BaseDatabaseContext databaseContext)
            : base(databaseContext)
        {

        }
    }
}
