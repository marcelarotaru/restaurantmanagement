﻿using RestaurantManagement.Application.Interfaces.Persistance;
using RestaurantManagement.Domain.Item;
using RestaurantManagement.Persistance.Common;

namespace RestaurantManagement.Persistance.Item
{
    public class ItemRepository
        : Repository<ItemEntity>,
        IItemRepository
    {
        public ItemRepository(BaseDatabaseContext databaseContext)
            : base(databaseContext)
        {

        }
    }
}
