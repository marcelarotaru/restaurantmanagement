﻿using RestaurantManagement.Domain.Item;
using System.Data.Entity.ModelConfiguration;

namespace RestaurantManagement.Persistance.Item
{
    public class ItemConfiguration
        : EntityTypeConfiguration<ItemEntity>
    {
        public ItemConfiguration()
        {
            HasKey(p => p.Id);

            Property(p => p.Price)
                .HasPrecision(8, 4);
        }
    }
}
