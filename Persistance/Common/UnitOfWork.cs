﻿using RestaurantManagement.Application.Interfaces.Persistance;

namespace RestaurantManagement.Persistance.Common
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly BaseDatabaseContext _databaseContext;

        public UnitOfWork(BaseDatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public void Save()
        {
            _databaseContext.SaveChanges();
        }
    }
}
