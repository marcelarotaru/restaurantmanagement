﻿using RestaurantManagement.Application.Interfaces.Persistance;
using System.Data.Entity;
using System.Linq;

namespace RestaurantManagement.Persistance.Common
{
    public class Repository<T>
        : IRepository<T>
        where T : class
    {
        private readonly BaseDatabaseContext _databaseContext;

        public Repository(BaseDatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public void Add(T entity)
        {
            _databaseContext.Set<T>().Add(entity);
        }

        public T Get(int id)
        {
            return _databaseContext.Set<T>().Find(id);
        }

        public IQueryable<T> GetAll()
        {
            return _databaseContext.Set<T>();
        }

        public void Update(T entity)
        {
            _databaseContext.Set<T>().Attach(entity);
            _databaseContext.Entry(entity).State = EntityState.Modified;
        }
    }
}
