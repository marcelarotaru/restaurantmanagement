﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantManagement.Persistance.Common
{
    internal sealed class DatabaseMigrationConfiguration
        : DbMigrationsConfiguration<BaseDatabaseContext>
    {
        public DatabaseMigrationConfiguration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            ContextKey = "RestaurantManagement.Persistance.BaseDatabaseContext";
        }
    }
}
