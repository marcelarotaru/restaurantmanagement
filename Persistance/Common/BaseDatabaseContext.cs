namespace RestaurantManagement.Persistance.Common
{
    using RestaurantManagement.Domain.Item;
    using RestaurantManagement.Domain.Order;
    using RestaurantManagement.Domain.OrderItem;
    using RestaurantManagement.Domain.Table;
    using RestaurantManagement.Persistance.Item;
    using RestaurantManagement.Persistance.Order;
    using RestaurantManagement.Persistance.OrderItem;
    using RestaurantManagement.Persistance.Table;
    using System.Data.Entity;

    public class BaseDatabaseContext : DbContext
    {
        public BaseDatabaseContext()
            : base("name=BaseDatabaseContext")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<BaseDatabaseContext, DatabaseMigrationConfiguration>());
        }

        public virtual DbSet<ItemEntity> ItemEntities { get; set; }
        public virtual DbSet<OrderEntity> OrderEntities { get; set; }
        public virtual DbSet<OrderItemEntity> OrderItemEntities { get; set; }
        public virtual DbSet<TableEntity> TableEntities { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Configurations.Add(new ItemConfiguration());
            modelBuilder.Configurations.Add(new OrderConfiguration());
            modelBuilder.Configurations.Add(new OrderItemConfiguration());
            modelBuilder.Configurations.Add(new TableConfiguration());
        }
    }
}