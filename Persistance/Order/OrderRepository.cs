﻿using RestaurantManagement.Application.Interfaces.Persistance;
using RestaurantManagement.Domain.Order;
using RestaurantManagement.Persistance.Common;

namespace RestaurantManagement.Persistance.Order
{
    public class OrderRepository
        : Repository<OrderEntity>,
        IOrderRepository
    {
        public OrderRepository(BaseDatabaseContext databaseContext)
            : base(databaseContext)
        {

        }
    }
}
