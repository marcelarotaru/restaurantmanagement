﻿using RestaurantManagement.Domain.Order;
using System.Data.Entity.ModelConfiguration;

namespace RestaurantManagement.Persistance.Order
{
    public class OrderConfiguration
        : EntityTypeConfiguration<OrderEntity>
    {
        public OrderConfiguration()
        {
            HasKey(p => p.Id);

            Property(p => p.TotalValue)
                .HasPrecision(8, 4);
        }
    }
}
