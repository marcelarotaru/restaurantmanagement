﻿using RestaurantManagement.Domain.Table;
using System.Data.Entity.ModelConfiguration;

namespace RestaurantManagement.Persistance.Table
{
    public class TableConfiguration
        : EntityTypeConfiguration<TableEntity>
    {
        public TableConfiguration()
        {
            HasKey(p => p.Id);
        }
    }
}
