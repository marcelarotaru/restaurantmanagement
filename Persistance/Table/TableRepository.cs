﻿using RestaurantManagement.Application.Interfaces.Persistance;
using RestaurantManagement.Domain.Table;
using RestaurantManagement.Persistance.Common;

namespace RestaurantManagement.Persistance.Table
{
    public class TableRepository : Repository<TableEntity>, ITableRepository
    {
        public TableRepository(BaseDatabaseContext databaseContext)
            : base(databaseContext)
        {

        }
    }
}
