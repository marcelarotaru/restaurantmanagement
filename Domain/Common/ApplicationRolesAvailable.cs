﻿namespace RestaurantManagement.Domain.Common
{
    public struct ApplicationRolesAvailable
    {
        public const string Admin = "Admin";
        public const string Waiter = "Waiter";
    }
}
