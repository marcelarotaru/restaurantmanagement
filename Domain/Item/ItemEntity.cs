﻿using RestaurantManagement.Domain.Common;

namespace RestaurantManagement.Domain.Item
{
    public class ItemEntity : BaseEntity
    {
        private ItemEntity()
        { 
        }

        private ItemEntity(int id, string name, string description, decimal price)
        {
            this.Id = id;
            this.Name = name;
            this.Description = description;
            this.Price = price;
        }

        public static ItemEntity Create(int id, string name, string description, decimal price)
        {
            return new ItemEntity(id, name, description, price);
        }

        public static ItemEntity Create(string name, string description, decimal price)
        {
            return ItemEntity.Create(0, name, description, price);
        }

        public string Name
        {
            get; private set;
        }

        public decimal Price
        {
            get; private set;
        }

        public string Description
        {
            get; private set;
        }
    }
}
