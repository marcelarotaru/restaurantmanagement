﻿namespace RestaurantManagement.Domain.Order
{
    public enum OrderState
    {
        Created,
        InProgress,
        Paid
    }
}
