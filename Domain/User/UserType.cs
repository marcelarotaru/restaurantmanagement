﻿namespace RestaurantManagement.Domain.User
{
    public enum UserType
    {
        Waiter,
        Admin
    }
}
