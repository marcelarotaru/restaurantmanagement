﻿using RestaurantManagement.Domain.Common;

namespace RestaurantManagement.Domain.User
{
    public class UserEntity : BaseEntity
    {
        public string FirstName { get; private set; }

        public string LastName { get; private set; }

        public bool IsLoggedIn { get; private set; }

        public string ApplicationUserId { get; private set; }
    }
}
