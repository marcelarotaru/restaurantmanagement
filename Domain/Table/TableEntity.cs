﻿using RestaurantManagement.Domain.Common;
using RestaurantManagement.Domain.User;

namespace RestaurantManagement.Domain.Table
{
    public class TableEntity : BaseEntity
    {
        private TableEntity()
        {
        }
        
        private TableEntity(int id, int numberOfSeats, TableState tableState, string user)
        {
            this.Id = id;
            this.NumberOfSeats = numberOfSeats;
            this.TableState = tableState;
            this.User = user;
        }

        public static TableEntity Create(int numberOfSeats)
        {
            return TableEntity.Create(0, numberOfSeats);
        }

        public static TableEntity Create (int id, int numberOfSeats)
        {
            return TableEntity.Create(id, numberOfSeats, TableState.Deactivated);
        }
        
        public static TableEntity Create(int id, int numberOfSeats, TableState tableState)
        {
            return new TableEntity(id, numberOfSeats, tableState, string.Empty);
        }

        public static TableEntity Create(int id, int numberOfSeats, TableState tableState, string user)
        {
            return new TableEntity(id, numberOfSeats, tableState, user);
        }
        public static TableEntity Create(int id, int numberOfSeats,string user)
        { 

            return new TableEntity(id, numberOfSeats, TableState.Activated, user);
        }

        public int NumberOfSeats
        {
            get; private set;
        }

        public TableState TableState
        {
            get; private set;
        }

        public string User
        {
            get; private set;
        }

        public void AssignTableToUser(string user)
        {
            User = user;
        }

        public void SetNumberOfSeats(int numberOfSeats)
        {
            NumberOfSeats = numberOfSeats;
        }

        public void Activate()
        {
            this.TableState = TableState.Activated;
        }

        public void Deactivate()
        {
            this.TableState = TableState.Deactivated;
            this.User = null;
        }
    }
}
