﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantManagement.Application.Table.Queries.GetDeactivatedTableList
{
    public interface IGetDeactivatedTableListQuery
    {
        List<DeactivatedTableListItemModel> Execute();
    }
}
