﻿using RestaurantManagement.Application.Interfaces.Persistance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantManagement.Application.Table.Queries.GetDeactivatedTableList
{
    public class GetDeactivatedTableListQuery :IGetDeactivatedTableListQuery
    {
        private readonly ITableRepository _tableRepository;

        public GetDeactivatedTableListQuery(ITableRepository tableRepository)
        {
            _tableRepository = tableRepository;
        }

        public List<DeactivatedTableListItemModel> Execute()
        {
            List<DeactivatedTableListItemModel> deactivatedTablesList= new List<DeactivatedTableListItemModel>();
              var list= _tableRepository.GetAll()
                .Select(table => new DeactivatedTableListItemModel
                {
                    Id = table.Id,
                    NumberOfSeats = table.NumberOfSeats,
                    TableState = table.TableState
                }).ToList();
            foreach (var element in list)
                if (element.TableState == Domain.Table.TableState.Deactivated)
                    deactivatedTablesList.Add(element);

            return deactivatedTablesList;
        }
    }
}
