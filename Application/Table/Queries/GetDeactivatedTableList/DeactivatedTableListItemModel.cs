﻿using RestaurantManagement.Domain.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantManagement.Application.Table.Queries.GetDeactivatedTableList
{
    public class DeactivatedTableListItemModel
    {
       
            public int Id { get; set; }

            public TableState TableState { get; set; }

            public string TableStateDescription { get; set; }

            public int NumberOfSeats { get; set; }
        
    }
}
