﻿using RestaurantManagement.Domain.Table;

namespace RestaurantManagement.Application.Table.Queries.GetTableList
{
    public class TableListItemModel
    {
        public int Id { get; set; }

        public TableState TableState { get; set; }

        public string TableStateDescription { get; set; }

        public int NumberOfSeats { get; set; }
    }
}
