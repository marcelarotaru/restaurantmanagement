﻿using System.Collections.Generic;

namespace RestaurantManagement.Application.Table.Queries.GetTableList
{
    public interface IGetTableListQuery
    {
        List<TableListItemModel> Execute();
    }
}
