﻿using RestaurantManagement.Application.Interfaces.Persistance;
using System.Collections.Generic;
using System.Linq;

namespace RestaurantManagement.Application.Table.Queries.GetTableList
{
    public class GetTableListQuery : IGetTableListQuery
    {
        private readonly ITableRepository _tableRepository;

        public GetTableListQuery(ITableRepository tableRepository)
        {
            _tableRepository = tableRepository;
        }

        public List<TableListItemModel> Execute()
        {
            return _tableRepository.GetAll()
                .Select(table => new TableListItemModel
                {
                    Id = table.Id,
                    NumberOfSeats = table.NumberOfSeats,
                    TableState = table.TableState
                }).ToList();
        }
    }
}
