﻿namespace RestaurantManagement.Application.Table.Queries.GetTableDetail
{
    public interface IGetTableDetailQuery
    {
        TableDetailModel Execute(int id);
    }
}
