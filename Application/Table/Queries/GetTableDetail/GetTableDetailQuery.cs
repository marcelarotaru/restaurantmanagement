﻿using RestaurantManagement.Application.Interfaces.Persistance;

namespace RestaurantManagement.Application.Table.Queries.GetTableDetail
{
    public class GetTableDetailQuery : IGetTableDetailQuery
    {
        private readonly ITableRepository _tableRepository;

        public GetTableDetailQuery(ITableRepository tableRepository)
        {
            _tableRepository = tableRepository;
        }

        public TableDetailModel Execute(int id)
        {
            var table = _tableRepository
                .Get(id);

            if (table != null)
            {
                return new TableDetailModel
                {
                    Id = table.Id,
                    NumberOfSeats = table.NumberOfSeats,
                    TableState = table.TableState.ToString()
                };
            }

            return null;
        }
    }
}
