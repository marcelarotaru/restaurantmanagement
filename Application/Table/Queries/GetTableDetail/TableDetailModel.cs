﻿namespace RestaurantManagement.Application.Table.Queries.GetTableDetail
{
    public class TableDetailModel
    {
        public int Id { get; set; }

        public string TableState { get; set; }

        public int NumberOfSeats { get; set; }
    }
}
