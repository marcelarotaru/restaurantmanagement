﻿namespace RestaurantManagement.Application.Table.Command.UpdateTable
{
    public interface IUpdateTableCommand
    {
        void Execute(UpdateTableModel model);
    }
}
