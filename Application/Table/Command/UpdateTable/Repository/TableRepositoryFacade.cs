﻿using RestaurantManagement.Application.Interfaces.Persistance;
using RestaurantManagement.Domain.Table;

namespace RestaurantManagement.Application.Table.Command.UpdateTable.Repository
{
    public class TableRepositoryFacade : ITableRepositoryFacade
    {
        private readonly ITableRepository _tableRepository;

        public TableRepositoryFacade(ITableRepository tableRepository)
        {
            _tableRepository = tableRepository;
        }

        public void UpdateTable(TableEntity table)
        {
            _tableRepository.Update(table);
        }
    }
}
