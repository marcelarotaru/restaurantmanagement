﻿using RestaurantManagement.Application.Interfaces.Persistance;
using RestaurantManagement.Application.Table.Command.UpdateTable.Factory;
using RestaurantManagement.Application.Table.Command.UpdateTable.Repository;

namespace RestaurantManagement.Application.Table.Command.UpdateTable
{
    public class UpdateTableCommand : IUpdateTableCommand
    {
        private readonly ITableRepositoryFacade _tableRepositoryFacade;
        private readonly ITableFactory _tableFactory;
        private readonly IUnitOfWork _unitOfWork;

        public UpdateTableCommand(
            ITableRepositoryFacade tableRepositoryFacade,
            ITableFactory tableFactory,
            IUnitOfWork unitOfWork)
        {
            _tableRepositoryFacade = tableRepositoryFacade;
            _tableFactory = tableFactory;
            _unitOfWork = unitOfWork;
        }

        public void Execute(UpdateTableModel model)
        {
            var table = _tableFactory
                .Create(model.Id, model.NumberOfSeats, model.TableState);

            _tableRepositoryFacade.UpdateTable(table);

            _unitOfWork.Save();
        }
    }
}
