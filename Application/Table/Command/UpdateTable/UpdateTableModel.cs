﻿namespace RestaurantManagement.Application.Table.Command.UpdateTable
{
    public class UpdateTableModel
    {
        public int Id { get; set; }

        public int TableState { get; set; }

        public int NumberOfSeats { get; set; }

        public string User { get; set; }
    }
}
