﻿using RestaurantManagement.Domain.Table;

namespace RestaurantManagement.Application.Table.Command.UpdateTable.Factory
{
    public class TableFactory : ITableFactory
    {
        public TableEntity Create(int id, int numberOfSeats, int tableState)
        {
            return TableEntity.Create(id, numberOfSeats, (TableState)tableState);
        }
    }
}
