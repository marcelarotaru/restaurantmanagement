﻿using RestaurantManagement.Domain.Table;

namespace RestaurantManagement.Application.Table.Command.UpdateTable.Factory
{
    public interface ITableFactory
    {
        TableEntity Create(int id, int numberOfSeats, int tableState);
    }
}
