﻿namespace RestaurantManagement.Application.Table.Command.ActivateTable.Repository
{
    using RestaurantManagement.Application.Interfaces.Persistance;
    using RestaurantManagement.Domain.Table;

    public class TableRepositoryFacade : ITableRepositoryFacade
    {
        private readonly ITableRepository _tableRepository;

        public TableRepositoryFacade(ITableRepository tableRepository)
        {
            _tableRepository = tableRepository;
        }

        public void UpdateTable(TableEntity table)
        {
            _tableRepository.Update(table);
        }
    }
}
