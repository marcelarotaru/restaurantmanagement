﻿using RestaurantManagement.Domain.Table;

namespace RestaurantManagement.Application.Table.Command.ActivateTable.Repository
{
    public interface ITableRepositoryFacade
    {
        void UpdateTable(TableEntity table);
    }
}
