﻿
namespace RestaurantManagement.Application.Table.Command.ActivateTable
{
   public interface IActivateTableCommand
    {
        void Execute(ActivateTableModel model);
    }
}
