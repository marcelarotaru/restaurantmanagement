﻿using RestaurantManagement.Application.Interfaces.Persistance;
using RestaurantManagement.Application.Table.Command.ActivateTable.Factory;
using RestaurantManagement.Application.Table.Command.ActivateTable.Repository;
using RestaurantManagement.Domain.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantManagement.Application.Table.Command.ActivateTable
{
    public class ActivateTableCommand : IActivateTableCommand
    {
        private readonly ITableRepositoryFacade _tableRepositoryFacade;
        private readonly ITableFactory _tableFactory;
        private readonly IUnitOfWork _unitOfWork;
      /*  In ActivateTableCommand, in metoda Execute ar trebui sa avem urmatorii pasi:
1. Identificarea mesei din modelul trimis
2. Alocarea userului logat mesei
3. Apelarea unui factory pentru Order, care sa returneze un order nou cu lista de item goala
4. Apelarea unei metode CreateOrder din fatada
5. Apelarea unei metode ActivateTable din fatada
6. Apelarea metodei Save din unitOfWork pentru a face modificarile persistente in baza de date.
*/
        public ActivateTableCommand(
           ITableRepositoryFacade tableRepositoryFacade,
           ITableFactory tableFactory,
           IUnitOfWork unitOfWork)
        {
            _tableRepositoryFacade = tableRepositoryFacade;
            _tableFactory = tableFactory;
            _unitOfWork = unitOfWork;
        }

        public void Execute(ActivateTableModel model)
        {
            var table = _tableFactory
                .Create(model.Id, model.NumberOfSeats, model.User);

            _tableRepositoryFacade.UpdateTable(table);

            _unitOfWork.Save();
        }
    }
}
