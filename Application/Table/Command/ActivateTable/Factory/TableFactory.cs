﻿using RestaurantManagement.Domain.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantManagement.Application.Table.Command.ActivateTable.Factory
{
    public class TableFactory : ITableFactory
    {
        public TableEntity Create(int id, int numberOfSeats, string user)
        {
            return TableEntity.Create(id, numberOfSeats, user);
        }

    }
}
