﻿using RestaurantManagement.Domain.Table;

namespace RestaurantManagement.Application.Table.Command.CreateTable.Repository
{
    public interface ITableRepositoryFacade
    {
        void AddTable(TableEntity table);
    }
}
