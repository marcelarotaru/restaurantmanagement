﻿using RestaurantManagement.Application.Interfaces.Persistance;
using RestaurantManagement.Domain.Table;

namespace RestaurantManagement.Application.Table.Command.CreateTable.Repository
{
    public class TableRepositoryFacade
        : ITableRepositoryFacade
    {
        private readonly ITableRepository _tableRepository;

        public TableRepositoryFacade(ITableRepository tableRepository)
        {
            _tableRepository = tableRepository;
        }
        
        public void AddTable(TableEntity table)
        {
            table.Deactivate();

            _tableRepository.Add(table);
        }
    }
}
