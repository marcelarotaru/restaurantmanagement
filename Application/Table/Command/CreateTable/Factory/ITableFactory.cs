﻿using RestaurantManagement.Domain.Table;

namespace RestaurantManagement.Application.Table.Command.CreateTable.Factory
{
    public interface ITableFactory
    {
        TableEntity Create(int numberOfSeats);
    }
}
