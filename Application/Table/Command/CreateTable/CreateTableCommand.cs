﻿using RestaurantManagement.Application.Interfaces.Persistance;
using RestaurantManagement.Application.Table.Command.CreateTable.Factory;
using RestaurantManagement.Application.Table.Command.CreateTable.Repository;

namespace RestaurantManagement.Application.Table.Command.CreateTable
{
    public class CreateTableCommand
        : ICreateTableCommand
    {
        private readonly ITableRepositoryFacade _tableRepositoryFacade;
        private readonly ITableFactory _tableFactory;
        private readonly IUnitOfWork _unitOfWork;

        public CreateTableCommand(
            ITableRepositoryFacade tableRepositoryFacade,
            ITableFactory tableFactory,
            IUnitOfWork unitOfWork)
        {
            _tableRepositoryFacade = tableRepositoryFacade;
            _tableFactory = tableFactory;
            _unitOfWork = unitOfWork;
        }

        public void Execute(CreateTableModel model)
        {
            var table = _tableFactory
                .Create(model.NumberOfSeats);

            _tableRepositoryFacade.AddTable(table);

            _unitOfWork.Save();
        }
    }
}
