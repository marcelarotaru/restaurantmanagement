﻿namespace RestaurantManagement.Application.Table.Command.CreateTable
{
    public class CreateTableModel
    {
        public int NumberOfSeats { get; set; }
    }
}
