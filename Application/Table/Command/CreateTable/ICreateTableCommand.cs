﻿namespace RestaurantManagement.Application.Table.Command.CreateTable
{
    public interface ICreateTableCommand
    {
        void Execute(CreateTableModel model);
    }
}
