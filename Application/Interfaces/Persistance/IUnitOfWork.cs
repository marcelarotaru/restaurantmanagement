﻿namespace RestaurantManagement.Application.Interfaces.Persistance
{
    public interface IUnitOfWork
    {
        void Save();
    }
}
