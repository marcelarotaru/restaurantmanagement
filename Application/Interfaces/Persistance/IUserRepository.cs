﻿using RestaurantManagement.Domain.User;

namespace RestaurantManagement.Application.Interfaces.Persistance
{
    public interface IUserRepository : IRepository<UserEntity>
    {
    }
}
