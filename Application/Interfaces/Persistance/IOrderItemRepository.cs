﻿using RestaurantManagement.Domain.OrderItem;

namespace RestaurantManagement.Application.Interfaces.Persistance
{
    public interface IOrderItemRepository : IRepository<OrderItemEntity>
    {
    }
}
