﻿using RestaurantManagement.Domain.Table;

namespace RestaurantManagement.Application.Interfaces.Persistance
{
    public interface ITableRepository : IRepository<TableEntity>
    {
      
    }
}
