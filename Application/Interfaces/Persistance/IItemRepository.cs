﻿using RestaurantManagement.Domain.Item;

namespace RestaurantManagement.Application.Interfaces.Persistance
{
    public interface IItemRepository : IRepository<ItemEntity>
    {
    }
}
