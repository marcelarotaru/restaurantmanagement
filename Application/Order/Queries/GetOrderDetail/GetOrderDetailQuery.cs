﻿using RestaurantManagement.Application.Interfaces.Persistance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantManagement.Application.Order.Queries.GetOrderDetail
{
    public class GetOrderDetailQuery : IGetOrderDetailQuery
    {
        private readonly IOrderRepository _orderRepository;
        public GetOrderDetailQuery(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;

        }


        public OrderDetailModel Execute(int id)
        {
            var order = _orderRepository.Get(id);

            if (order != null)
            {
                var result = new OrderDetailModel
                {
                    Id = order.Id,
                    TableID = order.Table.Id,
                    UserLogonID = order.User.ApplicationUserId,
                    OrderItemList = new List<OrderItemModel>()


                };
                foreach (var orderItem in order.ItemEntities)
                {
                    result.OrderItemList.Add(new OrderItemModel
                    {
                        ID = orderItem.Id,
                        Name = orderItem.Name,
                        Price = orderItem.Price
                    });
                }

                return result;
            }
            return null;
        }
    }
}
