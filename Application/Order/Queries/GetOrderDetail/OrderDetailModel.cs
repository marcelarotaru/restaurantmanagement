﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantManagement.Application.Order.Queries.GetOrderDetail
{
    public class OrderDetailModel
    {
        public int Id { get; set; }
        public List<OrderItemModel>OrderItemList { get; set; }
        public string UserLogonID { get; set; }
        public int TableID { get; set; }
        public decimal TotalValue { get; set; }

    }
}
