﻿using RestaurantManagement.Application.Interfaces.Persistance;

namespace RestaurantManagement.Application.Item.Queries.GetItemDetail
{
    public class GetItemDetailQuery : IGetItemDetailQuery
    {
        private readonly IItemRepository _itemRepository;

        public GetItemDetailQuery(IItemRepository itemRepository)
        {
            _itemRepository = itemRepository;
        }
        public ItemDetailModel Execute(int id)
        {
            var item = _itemRepository
                .Get(id);

            if (item != null)
            {
                return new ItemDetailModel
                {
                    Id = item.Id,
                    Description = item.Description,
                    Name = item.Name,
                    Price = item.Price.ToString()
                };
            }

            return null;
        }
    }
}
