﻿namespace RestaurantManagement.Application.Item.Queries.GetItemList
{
    public class ItemListModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Price { get; set; }
    }
}
