﻿using RestaurantManagement.Application.Interfaces.Persistance;
using System.Collections.Generic;
using System.Linq;

namespace RestaurantManagement.Application.Item.Queries.GetItemList
{
    public class GetItemListQuery : IGetItemListQuery
    {
        private readonly IItemRepository _itemRepository;

        public GetItemListQuery(IItemRepository itemRepository)
        {
            _itemRepository = itemRepository;
        }

        public List<ItemListModel> Execute()
        {
            return _itemRepository
                .GetAll()
                .Select(item => new ItemListModel
                {
                    Id = item.Id,
                    Name = item.Name,
                    Price = item.Price.ToString()
                }).ToList();
        }
    }
}
