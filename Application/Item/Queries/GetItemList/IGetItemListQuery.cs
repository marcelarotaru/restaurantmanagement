﻿using System.Collections.Generic;

namespace RestaurantManagement.Application.Item.Queries.GetItemList
{
    public interface IGetItemListQuery
    {
        List<ItemListModel> Execute();
    }
}
