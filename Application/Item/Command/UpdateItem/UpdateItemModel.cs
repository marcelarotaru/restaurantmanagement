﻿namespace RestaurantManagement.Application.Item.Command.UpdateItem
{
    public class UpdateItemModel
    {
        public int Id { get; set; }

        public string Description { get; set; }

        public string Name { get; set; }

        public decimal Price { get; set; }
    }
}
