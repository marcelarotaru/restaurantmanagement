﻿using RestaurantManagement.Application.Interfaces.Persistance;
using RestaurantManagement.Application.Item.Command.UpdateItem.Factory;
using RestaurantManagement.Application.Item.Command.UpdateItem.Repository;

namespace RestaurantManagement.Application.Item.Command.UpdateItem
{
    public class UpdateItemCommand : IUpdateItemCommand
    {
        private readonly IItemRepositoryFacade _itemRepository;
        private readonly IItemFactory _itemFactory;
        private readonly IUnitOfWork _unitOfWork;

        public UpdateItemCommand(
            IItemRepositoryFacade itemRepository,
            IItemFactory itemFactory,
            IUnitOfWork unitOfWork)
        {
            _itemRepository = itemRepository;
            _itemFactory = itemFactory;
            _unitOfWork = unitOfWork;
        }

        public void Execute(UpdateItemModel model)
        {
            var item = _itemFactory
                .Create(model.Id, model.Description, model.Name, model.Price);

            _itemRepository.UpdateItem(item);

            _unitOfWork.Save();
        }
    }
}
