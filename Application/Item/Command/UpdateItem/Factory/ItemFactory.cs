﻿using RestaurantManagement.Domain.Item;

namespace RestaurantManagement.Application.Item.Command.UpdateItem.Factory
{
    public class ItemFactory : IItemFactory
    {
        public ItemEntity Create(int id, string description, string name, decimal price)
        {
            return ItemEntity.Create(id, name, description, price);
        }
    }
}
