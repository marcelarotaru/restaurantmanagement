﻿using RestaurantManagement.Domain.Item;

namespace RestaurantManagement.Application.Item.Command.UpdateItem.Factory
{
    public interface IItemFactory
    {
        ItemEntity Create(int id, string description, string name, decimal price);
    }
}
