﻿namespace RestaurantManagement.Application.Item.Command.UpdateItem
{
    public interface IUpdateItemCommand
    {
        void Execute(UpdateItemModel model);
    }
}
