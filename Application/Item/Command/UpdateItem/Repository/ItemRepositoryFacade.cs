﻿using RestaurantManagement.Application.Interfaces.Persistance;
using RestaurantManagement.Domain.Item;

namespace RestaurantManagement.Application.Item.Command.UpdateItem.Repository
{
    public class ItemRepositoryFacade : IItemRepositoryFacade
    {
        private readonly IItemRepository _itemRepository;

        public ItemRepositoryFacade(IItemRepository itemRepository)
        {
            _itemRepository = itemRepository;
        }

        public void UpdateItem(ItemEntity item)
        {
            _itemRepository.Update(item);
        }
    }
}
