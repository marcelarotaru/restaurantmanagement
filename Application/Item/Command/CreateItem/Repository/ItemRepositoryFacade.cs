﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestaurantManagement.Application.Interfaces.Persistance;
using RestaurantManagement.Domain.Item;

namespace RestaurantManagement.Application.Item.Command.CreateItem.Repository
{
    public class ItemRepositoryFacade : IItemRepositoryFacade
    {
        private readonly IItemRepository _itemRepository;

        public ItemRepositoryFacade(IItemRepository itemRepository)
        {
            _itemRepository = itemRepository;
        }

        public void AddItem(ItemEntity item)
        {
            _itemRepository.Add(item);
        }
    }
}
