﻿using RestaurantManagement.Domain.Item;

namespace RestaurantManagement.Application.Item.Command.CreateItem.Repository
{
    public interface IItemRepositoryFacade
    {
        void AddItem(ItemEntity item);
    }
}
