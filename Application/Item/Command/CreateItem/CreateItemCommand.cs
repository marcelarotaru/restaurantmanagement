﻿using RestaurantManagement.Application.Interfaces.Persistance;
using RestaurantManagement.Application.Item.Command.CreateItem.Factory;
using RestaurantManagement.Application.Item.Command.CreateItem.Repository;
using System;

namespace RestaurantManagement.Application.Item.Command.CreateItem
{
    public class CreateItemCommand : ICreateItemCommand
    {
        private readonly IItemRepositoryFacade _itemRepositoryFacade;
        private readonly IItemFactory _itemFactory;
        private readonly IUnitOfWork _unitOfWork;

        public CreateItemCommand(
            IItemRepositoryFacade itemRepositoryFacade,
            IItemFactory itemFactory,
            IUnitOfWork unitOfWork)
        {
            _itemRepositoryFacade = itemRepositoryFacade;
            _itemFactory = itemFactory;
            _unitOfWork = unitOfWork;
        }


        public void Execute(CreateItemModel model)
        {
            var item = _itemFactory
                .Create(model.Description, model.Name, model.Price);

            _itemRepositoryFacade.AddItem(item);

            _unitOfWork.Save();
        }
    }
}
