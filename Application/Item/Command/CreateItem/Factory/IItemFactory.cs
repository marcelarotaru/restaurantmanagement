﻿using RestaurantManagement.Domain.Item;

namespace RestaurantManagement.Application.Item.Command.CreateItem.Factory
{
    public interface IItemFactory
    {
        ItemEntity Create(string description, string name, decimal price);
    }
}
