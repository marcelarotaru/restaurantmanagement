﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestaurantManagement.Domain.Item;

namespace RestaurantManagement.Application.Item.Command.CreateItem.Factory
{
    public class ItemFactory : IItemFactory
    {
        public ItemEntity Create(string description, string name, decimal price)
        {
            return ItemEntity.Create(name, description, price);
        }
    }
}
