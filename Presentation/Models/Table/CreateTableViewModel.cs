﻿using RestaurantManagement.Application.Table.Command.CreateTable;

namespace RestaurantManagement.Presentation.Models.Table
{
    public class CreateTableViewModel
    {
        public CreateTableModel Table { get; set; }
    }
}