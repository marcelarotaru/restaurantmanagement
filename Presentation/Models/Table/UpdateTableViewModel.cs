﻿using RestaurantManagement.Application.Table.Command.UpdateTable;

namespace RestaurantManagement.Presentation.Models.Table
{
    public class UpdateTableViewModel
    {
        public UpdateTableModel Table { get; set; }
    }
}