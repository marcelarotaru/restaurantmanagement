﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RestaurantManagement.Application.Table.Command.ActivateTable;

namespace RestaurantManagement.Presentation.Models.Table
{
    public class ActivateTableViewModel
    {
        public ActivateTableModel Table {get; set;}

    }
}