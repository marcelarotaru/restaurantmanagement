﻿using RestaurantManagement.Application.Table.Queries.GetTableDetail;

namespace RestaurantManagement.Presentation.Models.Table
{
    public class TableDetailViewModel
    {
        public TableDetailModel Table { get; set; }
    }
}