﻿using RestaurantManagement.Application.Item.Command.UpdateItem;

namespace RestaurantManagement.Presentation.Models.Item
{
    public class UpdateItemViewModel
    {
        public UpdateItemModel Item { get; set; }
    }
}