﻿using RestaurantManagement.Application.Item.Command.CreateItem;

namespace RestaurantManagement.Presentation.Models.Item
{
    public class CreateItemViewModel
    {
        public CreateItemModel Item { get; set; }
    }
}