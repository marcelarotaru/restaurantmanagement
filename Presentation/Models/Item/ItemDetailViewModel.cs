﻿using RestaurantManagement.Application.Item.Queries.GetItemDetail;
using System.Collections.Generic;
using System.Web.Mvc;

namespace RestaurantManagement.Presentation.Models.Item
{
    public class ItemDetailViewModel
    {
        public ItemDetailModel Item { get; set; }
    }
}