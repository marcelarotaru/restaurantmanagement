﻿using RestaurantManagement.Application.Table.Command.ActivateTable;
using RestaurantManagement.Application.Table.Command.CreateTable;
using RestaurantManagement.Application.Table.Command.UpdateTable;
using RestaurantManagement.Application.Table.Queries.GetDeactivatedTableList;
using RestaurantManagement.Application.Table.Queries.GetTableDetail;
using RestaurantManagement.Application.Table.Queries.GetTableList;
using RestaurantManagement.Domain.Common;
using RestaurantManagement.Domain.Table;
using RestaurantManagement.Presentation.Models.Table;
using System;
using System.Web.Mvc;

namespace RestaurantManagement.Presentation.Controllers
{
    [RoutePrefix("table")]
    public class TableController : Controller
    {
        private readonly IGetTableListQuery _tableListQuery;
        private readonly IGetDeactivatedTableListQuery _deactivatedTableListQuery;
        private readonly IGetTableDetailQuery _tableDetailQuery;
        private readonly ICreateTableCommand _createCommand;
        private readonly IUpdateTableCommand _updateCommand;
        private readonly IActivateTableCommand _activateCommand;

        public TableController(
            IGetTableListQuery tableListQuery,
            IGetDeactivatedTableListQuery deactivatedTableListQuery,
            IGetTableDetailQuery tableDetailQuery,
            ICreateTableCommand createCommand,
            IUpdateTableCommand updateCommand,
            IActivateTableCommand activateCommand
           )
        {
            _tableListQuery = tableListQuery;
            _tableDetailQuery = tableDetailQuery;
            _createCommand = createCommand;
            _updateCommand = updateCommand;
            _activateCommand = activateCommand;
            _deactivatedTableListQuery = deactivatedTableListQuery;
        }

        [Route("")]
        [HttpGet]
        [Authorize(Roles = ApplicationRolesAvailable.Admin)] 
        public ActionResult Index()
        {
            var model = _tableListQuery.Execute();

            return View(model);
        }

        [HttpGet]
        [Authorize(Roles = ApplicationRolesAvailable.Admin)]
        public ActionResult ListTables()
        {
            var model = _tableListQuery.Execute();

            return View(model);
        }
        
        [HttpGet]
        [Authorize(Roles = ApplicationRolesAvailable.Waiter)]
        public ActionResult ListDeactivatedTables()
        {
            var model =_deactivatedTableListQuery.Execute();


            return View(model);
        }

        // GET: Table/Details/5
        [Route("details/{id}")]
        [HttpGet]
        [Authorize(Roles = ApplicationRolesAvailable.Admin)]
        public ActionResult Details(int id)
        {
            var table = _tableDetailQuery.Execute(id);

            var model = new TableDetailViewModel
            {
                Table = table
            };

            return View(model);
        }

        [Route("create")]
        [HttpGet]
        [Authorize(Roles = ApplicationRolesAvailable.Admin)]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Table/Create
        [Route("create")]
        [HttpPost]
        [Authorize(Roles = ApplicationRolesAvailable.Admin)]
        public ActionResult Create(CreateTableViewModel viewModel)
        {
            try
            {
                var model = viewModel.Table;

                _createCommand.Execute(model);

                return RedirectToAction("index", "table");
            }
            catch
            {
                return View();
            }
        }

        [Route("edit/{id}")]
        [HttpGet]
        [Authorize(Roles = ApplicationRolesAvailable.Admin)]
        public ActionResult Edit(int id)
        {
            var table = _tableDetailQuery.Execute(id);

            var model = new UpdateTableViewModel
            {
                Table = new UpdateTableModel
                {
                    Id = table.Id,
                    NumberOfSeats = table.NumberOfSeats,
                    TableState = (int)Enum.Parse(typeof(TableState), table.TableState)
                }
            };

            return View(model);
        }

        // POST: Table/Edit/5
        [HttpPost]
        [Route("edit")]
        [Authorize(Roles = ApplicationRolesAvailable.Admin)]
        public ActionResult Edit(UpdateTableViewModel viewModel)
        {
            try
            {
                var model = viewModel.Table;

                _updateCommand.Execute(model);

                return RedirectToAction("index", "table");
            }
            catch
            {
                return View();
            }
        }
        // GET: Table/Activate/5
        [Route("activate/{id}")]
        [HttpGet]
        [Authorize(Roles = ApplicationRolesAvailable.Waiter)]
        public ActionResult Activate(int id)
        {
            var table = _tableDetailQuery.Execute(id);
            var model = new ActivateTableViewModel
            {
                Table = new ActivateTableModel
                {
                    Id = table.Id,                    
                    NumberOfSeats = table.NumberOfSeats,
                    TableState = (int)Enum.Parse(typeof(TableState), table.TableState)
                }
            };

            return View(model);
        }



        // POST: Table/Activate
        [HttpPost]
        [Route("activate")]
        [Authorize(Roles = ApplicationRolesAvailable.Waiter)] 
        public ActionResult Activate(ActivateTableViewModel viewModel)
        {

            try
            {
                var model = viewModel.Table;

                _activateCommand.Execute(model);

                return RedirectToAction("index", "table");
            }
            catch
            {
                return View();
            }

        }

        // GET: Table/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Table/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
       
    }
}
