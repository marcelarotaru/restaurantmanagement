﻿using System.Web.Mvc;

namespace RestaurantManagement.Presentation
{
    public class CustomRazorViewEngine : RazorViewEngine
    {
        public CustomRazorViewEngine()
        {
            ViewLocationFormats = new string[]
                {
                "~/{1}/Views/{0}.cshtml",
                };

            PartialViewLocationFormats = new string[]
                {
                "~/{1}/Views/{0}.cshtml",
                "~/Shared/Views/{0}.cshtml"
                };
        }
    }
}